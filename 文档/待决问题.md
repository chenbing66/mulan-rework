高优先在前

### 语法部分

[木兰编程语言待重现语法和功能](https://zhuanlan.zhihu.com/p/176769490)

#### 待确认

- 支持函数指定返回数组类型：`func shout(id): Person [A, B]`
- 超过两项赋值: a, b, c = 1, 2, 3
- [变长参数](http://www.yourownlinux.com/2016/12/python-function-args-kwargs.html#:~:text=Variable%20Length%20Arguments%20A%20Python%20function%20can%20use,function%20can%20accept%20keyworded%20variable%20length%20argument%20list.) (*args)
- 不支持直接初始化元组(tuple), 但可以由函数返回元组, 再赋值
- 是否支持slice: `print([1, 2, 3, 4, 5][slice(1, 3)])`
- `print(列表[0, 2, 3])` 报错："TypeError: list indices must be integers or slices, not tuple"

### 反馈信息中文化

- '测试/错误处理/词不识.ul', 无行号信息
- 引用带.木兰模块. 待确认: 模块名不能为'module'否则报语法错误?
- 引用木兰模块时, 如果模块有语法错误, 应该提示该语法错误与位置
  - 引用 python 模块时, 报错: `invalid syntax (test_module_py.py, line 7)`, 但引用木兰时仅报错`No module named xxx`

- 如果少了个匹配的 }:
```
分析器.错误.语法错误: 文件 "测试/实用/规划/形状.ul", 第1行, 第1列, 没认出这个词 "$end"
// [x, y]
^
```

[
  1,   // 多了逗号
]
```
分析器.错误.语法错误: 文件 "测试/实用/规划/形状.ul", 第27行, 第3列, 没认出这个词 "]"
  ],
  ^
```

需要输出错误类型, 比如`ValueError`:
```
>>> [1, 1].index(2)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: 2 is not in list
```

### 其他

- 编辑器
  - 显示当前打开文件的文件名
  - 检查不配对括号
  - 可以查看（搜索）当前测试用例
  - 记录编辑过程以及期间的建议（补全、警告、疑问、报错等等）
- 自动测试木兰代码
- 项目搭建 CI, 包括单元和集成测试
- 基于例程和 API 文档的组织的开发环境

### 不解

如果`using 所有歌 in 测试.儿歌`，运行`./中.py 测试/实用/搜索儿歌.ul`会运行所有测试用例，然后报错：
No module named '测试.儿歌'; '测试' is not a package
位于第1行：using 所有歌 in 测试.儿歌